<h1 align="center">
  <br>
  GitLab.com/im_elie_/arm64
  <br>
</h1>

<h4 align="center">Un ensemble de macros et de fonctions de base en arm64 pour Linux.</h4>

<p align="center">
  <a href="#contexte">Contexte</a> •
  <a href="#fonctions">Fonctions</a> •
  <a href="#wiki">Wiki</a> •
  <a href="#besoins-requirements">Besoins (Requirements)</a> •
  <a href="#utilisation">Utilisation</a> •
  <!---<a href="#exemples">Exemples</a> •--->
  <a href="#tests">Tests</a> •
  <a href="#license">License</a>
</p>

## Contexte
Ces implémentations s'inscrivent dans la phase de génération de code du projet de compilation de deuxième année à [TELECOM Nancy](https://telecomnancy.univ-lorraine.fr) : il s'agit de créer un compilateur du langage Tiger introduit par [Andrew W. Appel](https://www.cs.princeton.edu/~appel/) dans son livre *Modern Compiler Implementation*.\
Le GitHub du projet de compilation est disponible ici : [tiger-compiler](https://github.com/Tiger-compiler-team-2023/tiger-compiler).

## Fonctions
Le dépôt contient un ensemble de macros (`push`, `pop`, `exit`, `mod`, etc.) et de fonctions (arithmétiques, de gestion des tableaux, de gestions d'erreurs et d'interface avec l'entrée/sortie standard). Toutes ces macros et fonctions sont détaillées dans le <a href="#wiki">Wiki</a>.

## Wiki
Le Wiki est disponible [ici](wiki/wiki.md).

## Besoins (Requirements)
Vous aurez besoin d'une machine (ordinateur, serveur, ...) ayant un système d'exploitation [Linux](https://www.linux.org/) et une architecture de processeur `aarch64` (utilisez la commande `uname -p` pour connaître votre architecture).

Pour cloner et utiliser ce dépôt, vous aurez besoin de [Git](https://git-scm.com), de [make](https://www.gnu.org/software/make/) et de [Python](https://www.python.org/). 

## Utilisation
À la racine du projet vous trouverez un fichier d'exemple (`exemple.s`) qui affiche "Hello, world!" sur la sortie standard. Nous allons le compiler et le lancer.

Depuis votre terminal :

```bash
# Cloner le dépôt
$ git clone https://gitlab.com/im_elie_/arm64.git

# Aller dans le dépôt
$ cd arm64

# Compiler l'exemple
$ make

# Lancer l'exemple
$ ./exemple
```
<!---
## Exemples--->

## Tests
Des tests sont implémentés pour :
- L'affichage des erreurs à partir d'un code (cf. <a href="#wiki">Wiki</a>)
- Les fonctions arithmétiques
- Les fonctions de gestion des tableaux

L'ensemble des tests sont dans le dossier `tests`. On peut lancer l'ensemble des tests de la manière suivante :
```bash
# Lancer l'ensemble des tests
$ bash bash tout_test.sh
```

## License

[MIT](LICENSE.md)

---

<p align="center">
  Web <a href="https://www.goureau.eu/">www.goureau.eu</a> •
  GitLab <a href="https://gitlab.com/im_elie_">@im_elie_</a> •
  Mail <a href="mailto:git.arm64@theo.goureau.eu">git.arm64@theo.goureau.eu</a>
</p>

<pre style="margin-left: 42%; color: yellow;">
   __
  // \
  \\_/ //
 -(||)(')
  '''
</pre>
