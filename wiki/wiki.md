<h1 align="center">
  <br>
  <a href="https://gitlab.com/im_elie_/arm64">arm64</a> - Wiki
  <br>
</h1>

## Sommaire
|Partie  |Lien  |
|---  |---  |
|Conventions (Registres) | [Wiki/conventions](conventions.md)|
|Conventions (Appel de fonctions) | [Wiki/conventions](conventions.md)|
|Conventions (Code de retour) | [Wiki/conventions](conventions.md)|
|Contenu des fichiers de macros et fonctions  | [Wiki/macros-fonctions](macros-fonctions.md)|
|Utiliser les modules | [Voir ci-dessous](#utiliser-les-modules)|
|_Compiler_ le code assembleur  | [Voir ci-dessous](#compiler-le-code-assembleur)|
|Introduction aux outils de deboggage | [Wiki/gdb](gdb.md)|


---
## Utiliser les modules
Le mot-clé `include` permet de d'intégrer le contenu d'autres fichiers dans votre code et donc de découper votre code en modules. Un exemple de code important les trois modules est donnée ci-dessous.
```arm
.include    "base_macros.s"
// MACROS


// fin MACROS
.section    .text
.global     _start
_start:
// EXECUTION


// fin EXECUTION
	exit    #0
// FUNCTIONS


// fin FUNCTIONS
.include    "arithmetic_functions.s"
.include    "data_functions.s"
.include    "base_functions.s" // En dernier car débute la section data
// DATA


// fin DATA
```

---
## _Compiler_ le code assembleur
La compilation du code assembleur se fait en deux étapes : du code source au code objet puis du code objet à l'exécutable. Exemple avec le script `exemple.s`.
```bash
$	as -o exemple.o exemple.s -g    # source -> objet
$	ld -o exemple exemple.o         # object -> exécutable
```
NB : l'option `-g` permet d'utiliser l'outil de débogage présenté dans la partie suivante, elle n'est pas nécessaire.