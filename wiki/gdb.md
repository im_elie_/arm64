<h1 align="center">
  <br>
  <a href="https://gitlab.com/im_elie_/arm64">arm64</a> - Wiki/gdb
  <br>
</h1>

## Déboguer votre code arm64
Pour débogger, nous utilisons [gdb (GNU DeBugger)](https://www.sourceware.org/gdb/) et [gef](https://github.com/hugsy/gef).\

### Utilisation des outils
N'oubliez pas de compiler avec l'option `-g`.\
Lancer `gdb` et `gef` :
```bash
$  gdb exemple                     # exemple est l'exécutable
```
Ajouter un point d'arrêt en donnant un label :
```bash
gef➤  break _start                 # breakpoint au label _start
Breakpoint 1 at 0x4000b0: file exemple.s, line 11.
```
Lancer l'exécution :
```bash
gef➤  run
```
Vous verrez l'état des registres, l'état de la stack, la prochaine ligne à être exécutée et les données derrières les adresses dans les registres et dans la stack.\
Vous pouvez passer à la ligne suivante avec `step`.
```bash
gef➤  step
```
Vous pouvez quitter directement avec `Ctrl+D`.