<h1 align="center">
  <br>
  <a href="https://gitlab.com/im_elie_/arm64">arm64</a> - Wiki/macros-fonctions
  <br>
</h1>

### Les macros
Les macros sont contenues dans le fichier `base_macros.s`.
|Nom de la macro  |Utilisation  |
|--- |--- |
| `exit code` | (où code a une valeur entre 0 et 255) quitte l'exécution du processus avec le code `code`. |
| `push Xn` | empile le registre `Xn`. |
| `pop Xn` | désempile en écrivant la valeur dans le registre `Xn`. |
| `push915` | empile tous les registres de `x9` à `x15`. |
| `pop915` | désempile dans les registre de `x15` à `x9`. |
| `push1927` | empile tous les registres de `x19` à `x27`. |
| `pop1927` | désempile dans les registre de `x27` à `x19`. |
| `mod Xa, Xb, Xc` | écrit dans `Xa` le reste de la division euclidienne de `Xb` par `Xc`. |
| `print` | écrit sur la sortie standard le contenue de la chaîne de caractères à l'adresse `x1` de longueur `x2`. |
| `input` | lit sur la sortie standard et écrit dans le buffer d'adresse `x1` et de taille `x2` ; `x0` prend la valeur du nombre de caractères lus. |
| `err im` | gère les erreurs et exit avec `im` un code entre 0 et 255.

### Les fonctions de base
Les fonctions de base sont contenues dans le fichier `base_functions.s`.
|Nom de la fonction |#depile |#empile  |Utilisation  |
|--- |--- |--- |--- |
| `print_int16`  |1 arg | 0 res | écrit l'entier `arg1/1` sur la sortie standard en base 16 . |
| `print_int10`  |1 arg | 0 res | écrit l'entier `arg1/1` sur la sortie standard en base 10 . |
| `input_int10`  |0 arg | 1 res | lit jusqu'à 31 chiffres  (ou '-' et 30 chiffres) sur l'entrée standard et écrit la valeur de l'entier en base 10 lu au sommet de la pile. |
| `malloc`  |1 arg | 1 res | réserve `arg1/1` octets dans le tas et écrit l'adresse de l'espace alloué au sommet de la pile  (exit avec un statut 32 en cas d'erreur). |
| `error`  |1 arg | 0 res | gère les erreurs et exit avec le code `arg1/1` compris entre 0 et 255.

NB : Ce fichier contient une section data.

### Les fonctions sur les données
Les fonctions sur les données sont contenues dans le fichier `data_functions.s`.
|Nom de la fonction |#depile |#empile  |Utilisation  |
|--- |--- |--- |--- |
| `array_assign`  |2 arg | 1 res | (réserve le tableau dans le tas) écrit l'adresse du tableau de taille arg1/2 dont tous les éléments sont arg2/2 au sommet de la pile  (tests semantiques : taille >= 0 et bon fonctionnement de l'allocation mémoire). |
| `array_access`  |2 arg | 1 res | écrit l'adresse de la case du tableau d'adresse arg1/2 et d'indice arg2/2 au sommet de la pile (tests semantiques : indice >= 0 et indice < size). On utilise `str` et `ldr` pour modifier les valeurs des cellules du tableau, cette fonctions sert principalement aux tests sémantiques et à calculer le déplacement.

### Les fonctions arithmétiques
Les fonctions arithmétiques sont contenues dans le fichier `arithmetic_functions.s`.
|Nom de la fonction |#depile |#empile  |Utilisation  |
|--- |--- |--- |--- |
| `ari_int_neg`  |1 arg | 1 res | écrit l'opposé de `arg1/1` au sommet de la pile où `arg1/1` est un entier. |
| `ari_int_add`  |2 arg | 1 res | écrit `arg1/2` + `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_int_sub`  |2 arg | 1 res | écrit `arg1/2` - `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_int_mul`  |2 arg | 1 res | écrit `arg1/2` * `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_int_div`  |2 arg | 1 res | écrit `arg1/2` / `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. Gère le cas de la division par 0 avec un message d'erreur et un exit. |
| `ari_log_and`  |2 arg | 1 res | écrit `arg1/2` & `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des booléens  (entiers tels que 0 vaut faux et tout autre nombre vaut vrai). |
| `ari_log_or`  |2 arg | 1 res | écrit `arg1/2` \| `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des booléens  (entiers tels que 0 vaut faux et tout autre nombre vaut vrai). |
| `ari_int_EQ`  |2 arg | 1 res | écrit `arg1/2` = `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_int_NE`  |2 arg | 1 res | écrit `arg1/2` <> `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_int_GT`  |2 arg | 1 res | écrit `arg1/2` > `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_int_LT`  |2 arg | 1 res | écrit `arg1/2` < `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_int_GE`  |2 arg | 1 res | écrit `arg1/2` >= `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_int_LE`  |2 arg | 1 res | écrit `arg1/2` <= `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des entiers. |
| `ari_str_EQ`  |2 arg | 1 res | écrit `arg1/2` = `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des chaînes de caractères. |
| `ari_str_NE`  |2 arg | 1 res | écrit `arg1/2` <> `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des chaînes de caractères. |
| `ari_str_GT`  |2 arg | 1 res | écrit `arg1/2` > `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des chaînes de caractères. |
| `ari_str_LT`  |2 arg | 1 res | écrit `arg1/2` < `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des chaînes de caractères. |
| `ari_str_GE`  |2 arg | 1 res | écrit `arg1/2` >= `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des chaînes de caractères. |
| `ari_str_LE`  |2 arg | 1 res | écrit `arg1/2` <= `arg2/2` au sommet de la pile où `arg1/2` et `arg2/2` sont des chaînes de caractères.