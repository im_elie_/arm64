<h1 align="center">
  <br>
  <a href="https://gitlab.com/im_elie_/arm64">arm64</a> - Wiki/conventions
  <br>
</h1>

### Utilisation des registres
|Registre(s)  |Utilisation  |
|---  |---  |
|x0 - x7  | Paramètres et retours de fonctions  |
|x8 (XR) | Appels systèmes |
|x9 - x15 | Registres de travail qui doivent être empilés par l'appelant  |
|x19 - x27 | Registres de travail qui doivent être empilés par l'appelé  |
|x28 | Chaînage statique |
|x29 (FP) | Chaînage dynamique |
|x30 (LR) | Link register |

### Appel de fonction
|Déplacement  |Contenu  |Qui empile ?  |
|---  |---  |---  |
−2 | Ancien CS | Appelant
−1 | Ancien CD | Appelant
0 | Nouveau CS | Appelant
1 | arg1 | Appelant
2 | arg2 | Appelant
...|...|...
n | Adresse de retour | Appelé
n + 1 | Exécution puis résultat | Appelé

### Statuts de retour
|Statut  |Signification  |
|---  |---  |
|0  | Aucune erreur  |
|32  | Erreur d'allocation de mémoire (`malloc`)  |
|33  | Division par 0  |
|34  | Overflow lors d'un calcul arithmétique (n'est pas implémenté)  |
|35  | Index out of range  |
|36  | Taille ou indice négatif  |
|Autre  | Erreur inconnue  |