.include "base_macros.s"
// MACROS


// fin MACROS
.section .text
.global _start
_start:
// EXECUTION

    ldr     x1,     =hw     // x1 <- Adresse de la str
    push    x1              // empile x1
    bl      print_str       // appel print_str [1] -> [0]

// fin EXECUTION
	exit #0
// FUNCTIONS


// fin FUNCTIONS
.include "arithmetic_functions.s"
.include "data_functions.s"
.include "base_functions.s"
// DATA

hw:
    .asciz "Hello, world!\n"

// fin DATA
